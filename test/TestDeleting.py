import unittest
import os
import os.path

from propdb.propbag import PropBag
from propdb.propitem import PropItem


def delete_bag(bag):
    """ Deletes a PropBag. """

    if isinstance(bag, PropBag):
        if bag is not None and os.path.exists(bag.location):
            os.remove(bag.location)


class TestDeleting(unittest.TestCase):
    """ Test all the delete functionality. """

    def test_DeleteByFunction(self):
        """ Testing deleting property items via the Drop function. """

        bag = PropBag(self._testMethodName, auto_save= True)
        
        bagItems = ('item1', 1, 'item2', 2, 'item3', 3, 'item4', 4 \
            , 'item5', 5, 'item6', 6, 'item7', 7, 'item8', 8)

        # load bag
        bag.add(bagItems)

        bagItemCount = len(bag)

        # Drop item with just a name
        bag.drop('item1')
        bagItemCount -= 1
        self.assertEquals(len(bag), bagItemCount)
        
        # Drop items by lists
        bag.drop(('item2', 'item3'))
        bagItemCount -= 2
        self.assertEquals(len(bag), bagItemCount)
        bag.drop(['item4', 'item5'])
        bagItemCount -= 2
        self.assertEquals(len(bag), bagItemCount)
        
        # Drop item as PropItem
        bag.drop(PropItem('item6'))
        bagItemCount -= 1
        self.assertEquals(len(bag), bagItemCount)
        
        # Drop mixed items in list
        bag.drop(['item7', PropItem('item8')])
        bagItemCount -= 2
        self.assertEquals(len(bag), bagItemCount)

        # load bag
        bag.add(bagItems)

        bagItemCount = len(bag)

        # Try deleting an item where the name is not a string
        bag.drop(1)
        self.assertEquals(len(bag), bagItemCount)
        
        # Try deleting by list with names not as strings
        bag.drop((10, 11, 12, 13))
        self.assertEquals(len(bag), bagItemCount)
        
        # Empty bag
        bag.drop(bagItems)
        self.assertEquals(len(bag), 0)

        delete_bag(bag)

    def test_DeleteByOperator(self):
        """ Testing deleting property items via the - (subtract) operator. """

        bag = PropBag(self._testMethodName, auto_save= True)
        
        bagItems = ('item1', 1, 'item2', 2, 'item3', 3, 'item4', 4 \
            , 'item5', 5, 'item6', 6, 'item7', 7, 'item8', 8)

        # load bag
        bag.add(bagItems)

        bagItemCount = len(bag)

        # Drop item with just a name
        bag - 'item1'
        bagItemCount -= 1
        self.assertEquals(len(bag), bagItemCount)
        
        # Drop items by lists
        bag - ('item2', 'item3')
        bagItemCount -= 2
        self.assertEquals(len(bag), bagItemCount)
        bag - ['item4', 'item5']
        bagItemCount -= 2
        self.assertEquals(len(bag), bagItemCount)
        
        # Drop item as PropItem
        bag - PropItem('item6')
        bagItemCount -= 1
        self.assertEquals(len(bag), bagItemCount)
        
        # Drop mixed items in list
        bag - ['item7', PropItem('item8')]
        bagItemCount -= 2
        self.assertEquals(len(bag), bagItemCount)

        # load bag
        bag.add(bagItems)

        bagItemCount = len(bag)

        # Try deleting an item where the name is not a string
        bag - 1
        self.assertEquals(len(bag), bagItemCount)
        
        # Try deleting by list with names not as strings
        bag - (10, 11, 12, 13)
        self.assertEquals(len(bag), bagItemCount)
        
        # Empty bag
        bag - bagItems
        self.assertEquals(len(bag), 0)

        delete_bag(bag)

    def test_DeleteByOperator(self):
        """ Testing deleting property items via the -= (subtract equals) operator. """

        bag = PropBag(self._testMethodName, auto_save= True)
        
        bagItems = ('item1', 1, 'item2', 2, 'item3', 3, 'item4', 4 \
            , 'item5', 5, 'item6', 6, 'item7', 7, 'item8', 8)

        # load bag
        bag.add(bagItems)

        bagItemCount = len(bag)

        # Drop item with just a name
        bag -= 'item1'
        bagItemCount -= 1
        self.assertEquals(len(bag), bagItemCount)
        
        # Drop items by lists
        bag -= ('item2', 'item3')
        bagItemCount -= 2
        self.assertEquals(len(bag), bagItemCount)
        bag -= ['item4', 'item5']
        bagItemCount -= 2
        self.assertEquals(len(bag), bagItemCount)
        
        # Drop item as PropItem
        bag -= PropItem('item6')
        bagItemCount -= 1
        self.assertEquals(len(bag), bagItemCount)
        
        # Drop mixed items in list
        bag -= ['item7', PropItem('item8')]
        bagItemCount -= 2
        self.assertEquals(len(bag), bagItemCount)

        # load bag
        bag.add(bagItems)

        bagItemCount = len(bag)

        # Try deleting an item where the name is not a string
        bag -= 1
        self.assertEquals(len(bag), bagItemCount)
        
        # Try deleting by list with names not as strings
        bag -= (10, 11, 12, 13)
        self.assertEquals(len(bag), bagItemCount)
        
        # Empty bag
        bag -= bagItems
        self.assertEquals(len(bag), 0)

        delete_bag(bag)

if __name__ == '__main__':
    unittest.main()
