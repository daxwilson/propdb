#!/usr/bin/env python

import getpass

from propdb.propbag import PropBag
from propdb.propitem import PropItem


def init_cfg(bag):
    if 'initialized' not in bag:
        bag.add(('initialized', True, 'counter', 0, 'user', None, 'logged_in', False))


def print_cfg(bag):
    for item in bag:
        print("%s=%s" % (item.name, item.value))

if __name__ == '__main__':
    
    cfg = PropBag('config')

    init_cfg(cfg)
    
    # reset logged in
    cfg['logged_in'] = False

    print_cfg(cfg)

    # update run counter
    counter = cfg['counter']
    counter.value += 1
    cfg.set(counter)
    # or
    #cfg['counter'] = cfg['counter'].value + 1

    # update user
    user = PropItem('user', getpass.getuser())
    cfg.set(user)

    # update logged in
    cfg.set(('logged_in', True))

    print_cfg(cfg)
