#!/usr/bin/env python

import getpass
import sys
import os
from colorama import init, Fore, Back, Style

from propdb.propbag import PropBag
from propdb.propitem import PropItem

def add(bag, name, pwd):
    if name is None:
        name = raw_input('Name: ')

    if pwd is None:
        pwd = raw_input('Password: ')

    if name not in bag:
        newitems = bag.add((name, pwd))
        if len(newitems) > 0:
            print("%s successfully added." % newitems[0].name)
        else:
            print("%s failed to add." % name)
    else:
        print("Name '%s' already exists." % name)

def edit(bag, name, pwd):
    if name is None:
        name = raw_input('Name: ')

    if pwd is None:
        pwd = raw_input('New password: ')

    if name in bag:
        newitems = bag.set((name, pwd))
        if len(newitems) > 0:
            print("%s successfully edited." % newitems[0].name)
        else:
            print("%s failed to edit." % name)
    else:
        print("Name '%s' does not exist." % name)

def show(bag, name):
    if name is None:
        name = raw_input('Name: ')

    if name in bag:
        print('%s%s%s => %s%s%s' % (Fore.GREEN, name, Fore.RESET
            , Fore.YELLOW, mypwdbag[name].value, Fore.RESET))
    else:
        print("Name '%s' does not exist." % name)

def delete(bag, name):
    if name is None:
        name = raw_input('Name: ')

    if name in bag:
        if bag.drop(name) > 0:
            print("%s successfully dropped." % name)
        else:
            print("%s failed to drop." % name)
    else:
        print("Name '%s' does not exist." % name)

if __name__ == '__main__':
    
    init()

    print('Password Storage Program\n')
    print('Please login with your super secret password.')

    login_attempts = 1

    while True:
        is_login_good = False
        try:
            pwd = getpass.getpass('Password: ')
            mypwdbag = PropBag('protectedpwds', secret_key = pwd)
            is_login_good = True
        except:
            print('Failed to login...try again.')
            login_attempts += 1
        
        if is_login_good:
            break

        if login_attempts > 3:
            sys.exit(1)

    print('Login successful.')
    print("Enter 'help' to see available commands.\n")

    while True:
        try:
            input = raw_input('>>> ')
            
            cmds = input.split(' ', 2)
            cmds.extend((None, None))
            
            cmd = cmds[0].strip().lower()

            if cmd == 'exit':
                break
            elif cmd == 'help':
                print("Usuage: Command [optional] ...")
                print("  list")
                print("  show [name]")
                print("  add [name] [password]")
                print("  edit [name] [new password]")
                print("  delete or del [name]")
                print("  exit")
                print("  clear or clr")
                print("  help")
            elif cmd == 'list':
                for name in mypwdbag.names:
                    print('%s%s%s' % (Fore.GREEN, name, Fore.RESET))
            elif cmd == 'show':
                show(mypwdbag, cmds[1])
            elif cmd == 'add':
                add(mypwdbag, cmds[1], cmds[2])
            elif cmd == 'edit':
                edit(mypwdbag, cmds[1], cmds[2])
            elif cmd == 'delete' or cmd == 'del':
                delete(mypwdbag, cmds[1])
            elif cmd == 'clear' or cmd == 'clr':
                if sys.platform == "win32":
                    os.system('cls')
                else:
                    os.system('clear')
            elif cmd == '':
                pass
            else:
                print("%s is not a command." % cmd)

        except Exception as ex:
            print("Uh oh! %s" % str(ex))
    